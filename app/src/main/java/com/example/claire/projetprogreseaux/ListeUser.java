package com.example.claire.projetprogreseaux;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Anthony Whelan, Claire Bun
 */
public class ListeUser  implements Serializable
{
    //la classe pour les listes qui sont sauvegardes par l'utilisateur

    //Proprietes
    private String nomListe, username;
    //numero des films
    private ArrayList<Films> itemListe;

    //Constructeur
    public ListeUser(String nomListe, String username, ArrayList<Films> item) {
        this.nomListe = nomListe;
        this.username = username;
        this.itemListe = item;
    }

    //MEthode d'Access

    public ArrayList<Films> getItemListe() {
        return itemListe;
    }

    //methode toString

    @Override
    public String toString() {
        return "ListeUser{" +
                "nomListe='" + nomListe + '\'' +
                ", username='" + username + '\'' +
                ", item=" + itemListe +
                '}';
    }
}
