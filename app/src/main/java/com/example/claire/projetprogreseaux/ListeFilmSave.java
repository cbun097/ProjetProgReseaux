package com.example.claire.projetprogreseaux;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/*
 * Anthony Whelan, Claire Bun
 */
public class ListeFilmSave extends AppCompatActivity {

    //Varibles
    ListView listViewFilm;
    private ListeUser listeFilmSaves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_film_save);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Liste de films sauvegardés");
        listViewFilm = findViewById(R.id.listView_Films_save);
        Spinner s = (Spinner) findViewById(R.id.spinnerList);

        //la cle du Intent
        if (getIntent() != null && getIntent().getExtras().containsKey("listes"))
        {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getIntent().getStringArrayListExtra("listes"));
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            s.setAdapter(adapter);
        }

        //prendre le pays
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new ThreadClient.ThreadEnvoi(String.format("envoieListe::username=%s;liste=%s", Utils.CURRENT_USER, parent.getItemAtPosition(position).toString())).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listViewFilm.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InfosFilm.FILM = (Films)parent.getItemAtPosition(position);
                startActivity(new Intent(Utils.getCurrentActivity(), InfosFilm.class));
            }
        });
    }

    //Methode updateUI qui recoit le message du Ui
    //change la liste de film sauvegarde
    public void updateUI()
    {
        //afficher les elements de la liste dans la listeView
        ListeFilmAdapter adapter = new ListeFilmAdapter(this, Utils.CURRENT_SELECTED_LIST);
        listViewFilm.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

