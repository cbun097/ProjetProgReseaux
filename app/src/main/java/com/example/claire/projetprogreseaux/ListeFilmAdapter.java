package com.example.claire.projetprogreseaux;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/*
 * Anthony Whelan, Claire Bun
 */
public class ListeFilmAdapter extends ArrayAdapter<Films>
{
    private Context ctx;
    private ArrayList<Films> films;

    // Custom adapter pour afficher l'image du film
    public ListeFilmAdapter(Context ctx, ArrayList<Films> films)
    {
        super(ctx, R.layout.custom_liste, films);
        this.ctx = ctx;
        this.films = films;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        ViewHolder holder;
        Films f = getItem(position);
        if(convertView == null)
        {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(ctx).inflate(R.layout.custom_liste, parent, false);
            holder.titre = convertView.findViewById(R.id.titre);
            holder.poster = convertView.findViewById(R.id.poster);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.titre.setText(f.getNomFilm());
        // Méthode qui associer le poster au ImageView
        f.setPoster(holder.poster);

        return convertView;
    }

    public static class ViewHolder
    {
        public TextView titre;
        public ImageView poster;
    }
}
