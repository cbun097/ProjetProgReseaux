package com.example.claire.projetprogreseaux;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;

/*
 * Anthony Whelan, Claire Bun
 */
public class ThreadClient extends AsyncTask<String,String,Object> {
    //le cote thread client
    //demande de la connexion au server
    //SOURCE: https://developer.android.com/reference/android/os/AsyncTask.html

    //proprietes
    private int portLocal, portDistant;
    private String ipLocal, ipDistant;
    private BufferedReader in;
    private static PrintWriter out;

    //Constructeur
    public ThreadClient(int portLocal, String ipLocal, int portDistant, String ipDistant) {
        this.portLocal = portLocal;
        this.ipLocal = ipLocal;
        this.portDistant = portDistant;
        this.ipDistant = ipDistant;
    }

    //thread dans le background pour la connexion
    @Override
    protected Object doInBackground(String... params)
    {
        try
        {
            //creation du socket
            Socket socket = new Socket();
            //reutilise le port
            socket.setSoLinger(true, 0);
            //utiliser le port
            socket.setReuseAddress(true);
            //lier le socket
            socket.bind(new InetSocketAddress(this.ipLocal,2010));
            //la connexion au serveur
            socket.connect(new InetSocketAddress(this.ipDistant, 2011));
            Log.i("main", "Vous etes connecte au serveur.");

            // Create PrintWriter object for sending messages to server.
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

            //Create BufferedReader object for receiving messages from server.
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String msg;
            while ((msg = in.readLine()) != null)
            {
                publishProgress(msg);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values)
    {
        super.onProgressUpdate(values);
        String[] splits = values[0].split("::");
        //Les differents cas
        switch(splits[0])
        {
            case "connect" :
                if(splits[1].equals("true"))
                {
                    //si la connexion est approuvee, commencer la nouvelle activite
                    //partir l'activite pour faire la demande
                    Intent i = new Intent(Utils.getCurrentActivity(),DemandeFilm.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Utils.getCurrentActivity().startActivity(i);
                }
                else
                {
                    Toast.makeText(Utils.getCurrentActivity(), "Le compte n'existe pas", Toast.LENGTH_SHORT).show();
                }
                break;
            case "inscription" :
                if(splits[1].equals("true"))
                {
                    Toast.makeText(Utils.getCurrentActivity(),"l'inscription a ete un succes", Toast.LENGTH_SHORT).show();
                    Utils.getCurrentActivity().startActivity(new Intent(Utils.getCurrentActivity(), MainActivity.class));
                }
                else
                {
                    Toast.makeText(Utils.getCurrentActivity(), "l'utilisateur existe déjà", Toast.LENGTH_SHORT).show();
                }
                break;
            case "suggestion" :
                //partir le layout du film slider
                Intent i = new Intent(Utils.getCurrentActivity(), FilmsAleatoires.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("listeFilm", splits[1]);
                Utils.getCurrentActivity().startActivity(i);
                break;
            case "listeSave" :
                if(splits[1].equals("true"))
                {
                    Toast.makeText(Utils.getCurrentActivity(), "le film est ajoute", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Utils.getCurrentActivity(), "le film n'est pas ajoute", Toast.LENGTH_SHORT).show();
                }
                break;
            case "envoieListe" :
                Log.i("ThreadClient", "recevoir la liste save du serveur");
                Log.i("save" , " retour du serveur : " + splits[1]);

                // Pour télécharger les posters correctement dans l'objet film
                ArrayList<Films> listeServeur = ((ListeUser)new GsonBuilder().setLenient().create().fromJson(splits[1], new TypeToken<ListeUser>() {}.getType())).getItemListe();
                ArrayList<Films> listeConvertie = new ArrayList<>();
                for(Films f : listeServeur)
                {
                    Films filmConvert = new Films(f.getIdFilm(), f.getNomFilm(), f.getNomDirecteur(), f.getPaysOrigine(), f.getLanguesOrigine(), f.getDuree(), f.getRating(), f.getDateSorite(), f.getImageUrl(), f.getResume());
                    filmConvert.setListeActeurs(f.getListeActeurs());
                    listeConvertie.add(filmConvert);
                }

                Utils.CURRENT_SELECTED_LIST = listeConvertie;

                ((ListeFilmSave)Utils.getCurrentActivity()).updateUI();
                break;
            case "allListe" :
                Gson gson = new GsonBuilder().create();
                ArrayList<String> list = gson.fromJson(splits[1], new TypeToken<ArrayList<String>>(){}.getType());
                Log.i("save" , " retour du serveur : " + list);
                Intent in = new Intent(Utils.getCurrentActivity(), ListeFilmSave.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("listes", list);
                Utils.getCurrentActivity().startActivity(in);
                break;
        }
    }

    //La classe ThreadEnvoi
    public static class ThreadEnvoi extends AsyncTask<Void, Void, Void>
    {

        public String data;

        public ThreadEnvoi(String data)
        {
            this.data = data;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            envoyer(data);
            Log.i("ThreadClient", "data " + data);
            return null;
        }

        //methode qui envoit les messages au serveur
        public void envoyer(String data)
        {
            if (out != null)
            {
                out.println(data);
                out.flush();
            }
        }
    }
    //https://github.com/codepath/android_guides/wiki/Sending-and-Receiving-Data-with-Sockets
}
