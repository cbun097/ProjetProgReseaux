package com.example.claire.projetprogreseaux;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/*
 * Anthony Whelan, Claire Bun
 */
public class InfosFilm extends AppCompatActivity {

    public static Films FILM;
    private int BACKGROUND_COLOR = Color.rgb(213, 170, 64);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_info);
        setTitle("Informations du film");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView txt_nom_film = findViewById(R.id.txt_info_nom);
        TextView txt_duree = findViewById(R.id.txt_duree);
        TextView txt_pays = findViewById(R.id.info_pays);
        TextView txt_langue = findViewById(R.id.info_langue);
        TextView txt_description = findViewById(R.id.info_description);
        TextView txt_directeur = findViewById(R.id.info_nDirecteur2);
        TextView txt_actor = findViewById(R.id.txt_acteurs);
        TextView txt_rating = findViewById(R.id.txt_rating);
        ImageView img_film = findViewById(R.id.img_info);
        RelativeLayout layout = findViewById(R.id.relative_info);


        //Obtenir les infos du film
        if (FILM != null)
        {
            txt_nom_film.setText(FILM.getNomFilm());
            txt_duree.setText("Durée : " + String.valueOf(FILM.getDuree()));
            txt_pays.setText("Pays d'origine : " + FILM.getPaysOrigine());
            txt_langue.setText("Langue d'origine : " + FILM.getLanguesOrigine());
            txt_description.setText("Résumé : " + FILM.getResume());
            txt_directeur.setText("Nom du directeur : " + FILM.getNomDirecteur());
            txt_rating.setText("Rating : " + String.valueOf(FILM.getRating()));

            //la liste pour les acteurs
            String acteurs = "";

            for(String a : FILM.getListeActeurs())
                acteurs += a + ", ";

            txt_actor.setText("Acteurs, Actrice: " + acteurs);
            //telechargement de l'image via le url
            FILM.setPoster(img_film);
            //set le background pour le info film
            layout.setBackgroundColor(BACKGROUND_COLOR);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
