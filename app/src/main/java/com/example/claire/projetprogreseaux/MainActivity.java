package com.example.claire.projetprogreseaux;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/*
 * Anthony Whelan, Claire Bun
 */
public class MainActivity extends AppCompatActivity {

    //au login, la page de connexion

    private EditText txtUsername;
    private EditText txtPassword;
    private CheckBox checkBoxSave;
    private static final String NOM_PREF = "sharedPrefs";
    private SharedPreferences pref;
    private static ThreadClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnConnection = findViewById(R.id.btnConnection);
        Button btnCreercompte = findViewById(R.id.btnCreercompte);
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        checkBoxSave = findViewById(R.id.cbxSave);

        //appel de la methode load
        load();
        //Associer la variable avec ip address du device
        Utils.IP_LOCAL = ipDevice();
        //connexion au serveur
        startClient();
        //le listener pour le bouton connection
        btnConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!txtUsername.getText().toString().isEmpty() && !txtPassword.getText().toString().isEmpty())
                {
                    Utils.CURRENT_USER = txtUsername.getText().toString();
                    save();

                    // Fonction permettant la connection au serveur SI ELLE À ÉTÉ PERDUE
                    startClient();
                    new ThreadClient.ThreadEnvoi(String.format("connect::username=%s;password=%s", txtUsername.getText().toString(), txtPassword.getText().toString())).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        //le listener pour le bouton inscription
        btnCreercompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aller sur la nouveau page pour l'inscription
                Intent i = new Intent(MainActivity.this, Inscription.class);
                startActivity(i);
            }
        });
    }

    //Methode privee qui prends le ip de l'appareil
    //SOURCE: https://www.viralandroid.com/2016/01/how-to-get-ip-address-of-android-device-programmatically.html
    private String ipDevice()
    {
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        Log.i("main","adresse IP: " + ip);
        return ip;
    }

    //Methode qui active la connection avec le serveur
    public static void startClient()
    {
        if(client == null || client.getStatus() != AsyncTask.Status.RUNNING)
        {
            client = new ThreadClient(Utils.PORT_LOCAL, Utils.IP_LOCAL, Utils.PORT_DISTANT, Utils.IP_DISTANT);
            client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            Log.i("test", client.getStatus().name());
        }
    }

    //Methode save pour la sauvegarde du nom d'utilisateur et du mot de passe a l'entree de l'application
    private void save(){
        //SharedPreferences
        pref = getSharedPreferences(NOM_PREF, MODE_PRIVATE);
        pref.edit().putString("Id", (txtUsername.getText().toString()))
                    .putString("MotDePasse",(txtPassword.getText().toString()))
                    .putBoolean("Save",(checkBoxSave.isChecked())).commit();
    }

    //Methode load. Elle met ce que l'utilisateur a entré précédemment dans les edittext en sauvegarde
    private void load()
    {
        pref = getSharedPreferences(NOM_PREF,MODE_PRIVATE);
        if(pref.contains("Save") && pref.getBoolean("Save" , false)){

            if(pref.contains("Id"))
            {
                //la cle, la valeur par default
                ((EditText)findViewById(R.id.txtUsername)).setText(pref.getString("Id","Default"));
            }
            if(pref.contains("MotDePasse"))
            {
                ((EditText)findViewById(R.id.txtPassword)).setText(pref.getString("MotDePasse","Default"));
                ((CheckBox)findViewById(R.id.cbxSave)).setChecked(true);
            }
        }
    }
}
