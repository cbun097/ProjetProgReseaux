package com.example.claire.projetprogreseaux;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

/*
 * Anthony Whelan, Claire Bun
 */
public class DownloadImage extends AsyncTask<String, Void, Bitmap>
{
    //Proprietes
    private ImageView obj;
    private Films film;

    //Constructeur
    public DownloadImage(ImageView obj, Films film)
    {
        this.obj = obj;
        this.film = film;
    }

    //Methode doInBackground du Asyntask
    protected Bitmap doInBackground(String... urls)
    {
        String url = urls[0];
        Bitmap bitmap = null;
        try
        {
            InputStream in = new URL(url).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        }
        catch (Exception e)
        {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        obj.setImageBitmap(bitmap);
        film.setImage(bitmap);
    }
}
