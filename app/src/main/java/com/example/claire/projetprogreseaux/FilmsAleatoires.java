package com.example.claire.projetprogreseaux;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*
 * Anthony Whelan, Claire Bun
 */
public class FilmsAleatoires extends AppCompatActivity {

    //View pour la liste des films
    //Variables
    private ViewPager viewPager;
    private SlideAdapter myAdapter;
    private ArrayList<Films> films;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_films);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //creer la action bar, recupere la action bar
        //android.support.v7.app.ActionBar ab = getSupportActionBar();
        if(getIntent() != null && getIntent().getExtras().containsKey("listeFilm"))
        {
            Gson gson = new GsonBuilder().setLenient().create();
            films = gson.fromJson(getIntent().getExtras().getString("listeFilm"), new TypeToken<ArrayList<Films>>(){}.getType());
        }

        //SOURCE: https://github.com/aws1994/Slider
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        myAdapter = new SlideAdapter(this, films);
        viewPager.setAdapter(myAdapter);


        Log.i("Films", String.valueOf(films.size()));
        Log.i("Film", "films: " + films);

    }

    //La barre pour le menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            //dans la barre si le client touche l'icone de la liste
            case R.id.liste:
                new ThreadClient.ThreadEnvoi(String.format("allListe::username=%s", Utils.CURRENT_USER)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            //dans le barre si le client touche l'icone du relode
            case R.id.reload:
                // Fonction permettant la connection au serveur si elle à été perdu
                MainActivity.startClient();
                break;
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


