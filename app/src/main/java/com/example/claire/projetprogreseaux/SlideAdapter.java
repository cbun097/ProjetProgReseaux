package com.example.claire.projetprogreseaux;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/*
 * Anthony Whelan, Claire Bun
 */
//SOURCE: https://github.com/aws1994/Slider
public class SlideAdapter extends PagerAdapter {

    //Variables
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Films> list_films;
    private Button btnSave, btnCancel;
    private EditText txt_nom_list;

    // list of titles
    public String[] lst_title = {
            "",
            "",
            "",
            "",
            ""
    };

    // list of background colors
    public int[]  lst_backgroundcolor = {
            Color.rgb(55,55,55),
            Color.rgb(239,85,85),
            Color.rgb(110,49,89),
            Color.rgb(1,188,212),
            Color.rgb(40,49,89),
    };


    //Constructeur par parametre
    public SlideAdapter(Context context, ArrayList<Films> list_films) {

        this.context = context;
        this.list_films = list_films;
    }

    @Override
    public int getCount() {
        return lst_title.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.film_item,container,false);
        LinearLayout layoutslide = (LinearLayout) view.findViewById(R.id.slidelinearlayout);
        ImageView imgslide = (ImageView)  view.findViewById(R.id.slideimg);
        TextView txttitle= (TextView) view.findViewById(R.id.txttitle);
        TextView description = (TextView) view.findViewById(R.id.txtdescription);
        layoutslide.setBackgroundColor(lst_backgroundcolor[position]);

        //prendre l'image qui est en URL
        list_films.get(position).setPoster(imgslide);
        txttitle.setText(list_films.get(position).getNomFilm());
        description.setText(list_films.get(position).getResume());
        container.addView(view);

        //bouton info
        Button btnInfo = view.findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Films film = list_films.get(position);
                //affiche la page avec les informations supplementaires du film
                Intent i = new Intent(context.getApplicationContext(),InfosFilm.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                InfosFilm.FILM = film;
                context.startActivity(i);
                Log.i("SlideAdapter", "le film selectionne: " + list_films.get(position).getNomFilm());
            }
        });

        //bouton Ajouter
        //Ajoute le film dans une playlist
        Button btnAjouter = view.findViewById(R.id.btnAjouter);
        btnAjouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creation du dialog box pour que l'utilisateur entre le nom de la playlist
                final Dialog dialog = new Dialog(context); // Context, this, etc.
                dialog.setContentView(R.layout.dialogue_playlist);
                btnSave = dialog.findViewById(R.id.btnSave);
                btnCancel = dialog.findViewById(R.id.btnCancel);
                txt_nom_list = dialog.findViewById(R.id.txt_nom_list);
                dialog.show();

                //btnSave est clicke
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String insertliste = txt_nom_list.getText().toString();
                        String filmid = list_films.get(position).getIdFilm();
                        new ThreadClient.ThreadEnvoi(String.format("savefilm::nomListe=%s;username=%s;filmid=%s", insertliste, Utils.CURRENT_USER, filmid)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        Log.i("liste_film","film: " + list_films.get(position).getNomFilm());
                        dialog.dismiss();
                    }
                });

                //btnCancel
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}