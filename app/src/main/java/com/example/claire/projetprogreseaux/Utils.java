package com.example.claire.projetprogreseaux;

import android.app.Activity;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

/*
 * Anthony Whelan, Claire Bun
 */
public class Utils
{
    public static int PORT_LOCAL = 2010;
    public static int PORT_DISTANT = 2011;
    public static String IP_LOCAL = "";
    public static String IP_DISTANT = "192.168.0.161";
    public static String CURRENT_USER;
    public static ArrayList<Films> CURRENT_SELECTED_LIST = new ArrayList<>();

    // https://stackoverflow.com/questions/11411395/how-to-get-current-foreground-activity-context-in-android/28423385#28423385
    public static Activity getCurrentActivity()
    {
        try
        {
            Class activityThreadClass = Class.forName("android.app.ActivityThread");
            Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null);
            Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
            activitiesField.setAccessible(true);

            Map<Object, Object> activities = (Map<Object, Object>) activitiesField.get(activityThread);
            if (activities == null)
                return null;

            for (Object activityRecord : activities.values()) {
                Class activityRecordClass = activityRecord.getClass();
                Field pausedField = activityRecordClass.getDeclaredField("paused");
                pausedField.setAccessible(true);
                if (!pausedField.getBoolean(activityRecord)) {
                    Field activityField = activityRecordClass.getDeclaredField("activity");
                    activityField.setAccessible(true);
                    Activity activity = (Activity) activityField.get(activityRecord);
                    return activity;
                }
            }
        }
        catch(Exception e)
        {
            Log.e("Error", e.getMessage());
        }

        return null;
    }
}
