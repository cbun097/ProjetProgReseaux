package com.example.claire.projetprogreseaux;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Anthony Whelan, Claire Bun
 */
public class Films
{
    //La classe pour les films

    //Proprietes
    private String IdFilm, nomFilm, nomDirecteur, paysOrigine, languesOrigine, imageUrl, resume, dateSorite;
    private double duree;
    private int rating;
    private List<String> listeActeurs;
    private Bitmap image;

    //Constructeur par parametre
    public Films(String IdFilm, String nomFilm, String nomDirecteur, String paysOrigine, String languesOrigine, double duree, int rating, String dateSorite, String imageUrl, String resume)
    {
        this.IdFilm = IdFilm;
        this.nomFilm = nomFilm;
        this.nomDirecteur = nomDirecteur;
        this.paysOrigine = paysOrigine;
        this.languesOrigine = languesOrigine;
        this.duree = duree;
        this.rating = rating;
        this.dateSorite = dateSorite;
        this.imageUrl = imageUrl;
        this.resume = resume;
        //initialiser la liste
        listeActeurs = new ArrayList<>();
    }

    //Methode d'access
    public String getIdFilm() {
        return IdFilm;
    }

    public String getNomFilm() {
        return nomFilm;
    }

    public String getNomDirecteur() {
        return nomDirecteur;
    }

    public String getPaysOrigine() {
        return paysOrigine;
    }

    public String getLanguesOrigine() {
        return languesOrigine;
    }

    public double getDuree() {
        return duree;
    }

    public int getRating() {
        return rating;
    }

    public String getDateSorite() {
        return dateSorite;
    }

    public List<String> getListeActeurs() {
        return listeActeurs;
    }

    public void setListeActeurs(List<String> listeActeurs)
    {
        this.listeActeurs = listeActeurs;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getResume() {
        return resume;
    }

    public void setImage(Bitmap image)
    {
        this.image = image;
    }

    /**
     * Télécharge l'image et l'associe a l'imageview désiré
     * @param v l'image view
     */
    public void setPoster(ImageView v)
    {
        if(image == null)
            new DownloadImage(v, this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, this.getImageUrl());
        else
            v.setImageBitmap(image);
    }

    //Methode toString
    @Override
    public String toString() {
        return "film id: " + this.IdFilm + "nom film: " + this.nomFilm + "Nom directeur: " + this.nomDirecteur + " pays: " + paysOrigine +
                "langues origine: " + this.languesOrigine + "duree: " + this.duree + "rating: " + this.rating +
                "date de sortie: " + this.dateSorite + "image url: " + this.imageUrl +
                "resume: " + this.resume;
    }
}
