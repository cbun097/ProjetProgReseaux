package com.example.claire.projetprogreseaux;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/*
 * Anthony Whelan, Claire Bun
 */
public class DemandeFilm extends AppCompatActivity {

    //page qui est associe avec le bouton qui demarre la demande de film
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demande_film);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.btnDemande).setOnClickListener(v -> {
            // Envoi de la demande au serveur
            new ThreadClient.ThreadEnvoi("suggestion::ask").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        });
    }

    //La barre pour le menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){
            case R.id.reload:
                // Fonction permettant la connection au serveur si elle à été perdu
                MainActivity.startClient();
                break;
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
