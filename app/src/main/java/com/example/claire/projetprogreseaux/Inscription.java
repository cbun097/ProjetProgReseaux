package com.example.claire.projetprogreseaux;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/*
 * Anthony Whelan, Claire Bun
 */
public class Inscription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        //ajouter les pays dans le spinner
        //creer un ArrayAdapter
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.paysList, android.R.layout.simple_spinner_item);
        // specification du layout
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // affecter le spinner avec l'adapteur
        ((Spinner)findViewById(R.id.spinnerPays)).setAdapter(adapter);
        final EditText txtNom = (EditText) findViewById(R.id.txtNomInsc);
        final EditText txtPrenom = (EditText) findViewById(R.id.txtPrenomInsc);
        final EditText txtPass = (EditText) findViewById(R.id.txtPswInsc);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmailInsc);
        final EditText txtUser = (EditText) findViewById(R.id.txtUserInsc);
        final Spinner pays = (Spinner)findViewById(R.id.spinnerPays);


        //ajouter les usagers dans la base de donnees
        findViewById(R.id.btnInscription).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //envoyer les donnees dans la DB lors de l'inscription de l'utilisateur
                if(!txtNom.getText().toString().isEmpty() && !txtPrenom.getText().toString().isEmpty() && !txtPass.getText().toString().isEmpty() && !txtEmail.getText().toString().isEmpty() && !txtUser.getText().toString().isEmpty() && !pays.getSelectedItem().toString().isEmpty())
                {
                    // Fonction permettant la connection au serveur si elle à été perdu
                    MainActivity.startClient();
                    //le thread client qui envoit les donnees au serveur
                    new ThreadClient.ThreadEnvoi(String.format("inscription::user=%s;nom=%s;prenom=%s;password=%s;email=%s;pays=%s", txtUser.getText().toString(), txtNom.getText().toString(), txtPrenom.getText().toString(), txtPass.getText().toString(), txtEmail.getText().toString(), pays.getSelectedItem().toString())).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                else
                    //message d'erreur dans un toast
                    Toast.makeText(getApplicationContext(), "Il manque des informations!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
